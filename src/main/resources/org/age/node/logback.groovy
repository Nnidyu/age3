package org.age.node

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender
import org.age.services.logs.logback.HazelcastAppender

def bySecond = timestamp("yyyyMMdd'T'HHmmss")

appender("FILE", FileAppender) {
    file = "node-${bySecond}.log"
    append = false
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{40} - %msg%n"
    }
}

appender("CONSOLE", ConsoleAppender) {
    filter(ch.qos.logback.classic.filter.ThresholdFilter) {
        level = INFO
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%highlight(%.-1level) %green(%-40logger{39}) : %msg%n"
    }
}

appender("HZ", HazelcastAppender)

root(ALL, ["FILE", "HZ"])
logger("org.age", DEBUG, ["CONSOLE"])
logger("com.hazelcast", INFO)
logger("org.springframework", INFO)
